function setup(){
    createCanvas(displayWidth, displayHeight);
}

var beadwidth = 40;
var beadheight = 30;
var answerbarheight = 40;
var numrods = 17;
var rodtop = 20;
var rodheight = beadheight*7+answerbarheight;
var rodwidth = 10;

function draw(){
background(255,255,255);
var rodbasex = 20  
    var rodstartx = rodbasex;
    //draw each rod in the soroban
    for(let i = 0; i < numrods ;i++){
        fill(255,255,230);
        // draw the rod
        rect(rodstartx,rodtop-3,rodwidth,rodheight+7);
        
        // now see if our number has any heaven beads
        var numearthbeads = soroban[i];
        fill(4,0xbe,0xef);
        if ( numearthbeads > 4 ){
            //we have a heaven bead!
            ellipse(rodstartx+(rodwidth/2), rodtop+(beadheight/2)+beadheight,beadwidth,beadheight);
          numearthbeads -= 5; 
        }else{
            ellipse(rodstartx+(rodwidth/2), rodtop+(beadheight/2),beadwidth,beadheight);
        }
        var beadpos = rodtop+(beadheight/2)+(beadheight)+answerbarheight;
        fill(04,0xf0,0xb0);
        for(let i = 0;i < numearthbeads;i++){
            beadpos+=beadheight;
            ellipse(rodstartx+(rodwidth/2),beadpos,beadwidth,beadheight);
        }
        beadpos += beadheight;
        for(let i = 0; i < 4-numearthbeads;++i){
            
            beadpos+=beadheight;
            ellipse(rodstartx+(rodwidth/2),beadpos,beadwidth,beadheight);
        }
        rodstartx += rodwidth+beadwidth;
    }
    //draw the answer bar
    fill(255,255,230);
    var abstart = rodtop+10+answerbarheight+(beadheight/2);
    rect(0, abstart, rodstartx,answerbarheight-10);
    fill(0,0,0);
    //mark every third row with a black dot
    for(let i = soroban.length;i > 0;i--){
        if(ceil((i+1)/3) == (i+1)/3 && i < soroban.length) {
            ellipse(rodstartx+(rodwidth/2), abstart+(answerbarheight/2)-5, 10,10);
        }
      rodstartx -= rodwidth+beadwidth;
    }
    //Print a tally of the total
    var totalstring = "Total: ";
    var isSignificant = false
    for(let i = 0;i<soroban.length;i++){
        if(soroban[i]>0){
            isSignificant = true;
        }
        if(isSignificant || i == soroban.length-1){
            totalstring += soroban[i];
        }
    }
    textSize(32);
    text(totalstring, 20, rodtop+rodheight+50);
}

function mousePressed(){
    //see if we are in Rod country
    if(mouseX > 0 && mouseX < soroban.length*(beadwidth+rodwidth)){
        if(mouseY > rodtop && mouseY < rodtop+rodheight){
            //See which rod we are on.
            var rodnum = ceil(mouseX/(beadwidth+rodwidth));
            //See which beadplace we are on.
            var cellnum = floor(mouseY/beadheight);
          
            //Now we need to find out if we are on a heaven bead
            //spot number 3 is answer bar
            if(cellnum < 3){
                if(soroban[rodnum-1] > 4){
                    soroban[rodnum-1] -= 5;
                }else{
                    soroban[rodnum-1] += 5;
                }
            }else if(cellnum > 3){
                // for this, we need to do a bit of math
                if(soroban[rodnum-1] > 4){
                    soroban[rodnum-1] = (cellnum-4)+5;
                }else{
                    soroban[rodnum-1] = cellnum-4;
                }
            }
        }
    }
}
var soroban = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
